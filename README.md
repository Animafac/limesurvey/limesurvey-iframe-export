# LimeSurvey Iframe Export

This small plugin adds a button in LimeSurvey's admin
that provides an HTML snippet that can be used to integrate a survey into a webpage.

## Install

You need to put the plugin in the `plugins/` folder of your LimeSurvey instance and then enable it in the LimeSurvey admin.

## Usage

When editing a form, the plugin adds a new button in the *Tools* menu.

## JavaScript tools

Some useful JavaScript tools can be installed with [Yarn](https://yarnpkg.com/):

```bash
yarn install
```

[jsonlint](https://github.com/zaach/jsonlint) can be used to make sure JSON files are valid:

```bash
yarn jsonlint composer.json
yarn jsonlint package.json
```
