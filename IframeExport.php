<?php
/**
 * IframeExport class
 */

use LimeSurvey\Menu\MenuItem;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\ArrayLoader;

require_once __DIR__.'/vendor/autoload.php';

/**
 * Main plugin class
 */
class IframeExport extends PluginBase
{

    /**
     * Plugin description
     * @var string
     */
    static protected $description = 'Export a survey into an iframe';

    /**
     * Plugin name
     * @var string
     */
    static protected $name = 'IframeExport';

    /**
     * Translator instance
     * @var Translator
     */
    private $translator;

    /**
     * Locales
     * @var array[]
     */
    static private $locales = [
        'en' => array(
            'popupInfo' => 'Copy this HTML into your web page:',
            'menuLabel' => 'Integrate in a webpage'
        ),
        'fr' => array(
            'popupInfo' => 'Copiez le code HTML suivant dans votre page web :',
            'menuLabel' => 'Intégrer dans une page web'
        )
    ];

    /**
     * Yii app instance
     * @var LSYii_Application
     */
    private $app;

    /**
     * Initialize plugin
     * @return void
     */
    public function init()
    {
        $this->app = Yii::app();
        $this->translator = new Translator($this->app->getLanguage());
        $this->translator->setFallbackLocales(array('en'));
        $this->translator->addLoader('array', new ArrayLoader());

        foreach (self::$locales as $locale => $translations) {
            $this->translator->addResource('array', $translations, $locale);
        }

        $this->subscribe('beforeToolsMenuRender');
    }

    /**
     * Event called when rendering the Tools menu
     * @return void
     */
    public function beforeToolsMenuRender()
    {
        $event = $this->getEvent();

        $iframe = '<iframe src="'.
            $this->app->createAbsoluteUrl(
                'survey/index/sid/'.$event->get('surveyId').'/newtest/Y/lang/'.$this->app->getLanguage()
            ).
            '" style="border: none;" width="600" height="800"></iframe>';
        $js = 'alert("'.
            $this->translator->trans('popupInfo').
            '\n\n'.
            addslashes($iframe).
            '");';

        $event->append('menuItems', array(
            new MenuItem(array(
                'label'     => $this->translator->trans('menuLabel'),
                'href'      => "javascript:".htmlentities($js),
                'iconClass' => 'icon-copy'
            ))
        ));
    }
}
